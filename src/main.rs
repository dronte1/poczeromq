use std::mem::size_of;
use crate::result::PdvResult;
use async_zmq::zmq::Sendable;
use async_zmq::{publish, subscribe, SinkExt};
use futures::executor::{block_on, LocalPool};
use smol::stream::StreamExt;
use smol::{LocalExecutor, Task, Timer};
use std::time::Duration;

mod result;

#[derive(Debug)]
struct NoteValues {
    value: u8,
    velocity: u8,
    channel: u8,
}

#[derive(Debug)]
enum Note {
    On(NoteValues),
    Off(NoteValues),
}

#[derive(Debug)]
struct Continous {
    number: u32,
    value: u32,
    channel: u8,
}

#[derive(Debug)]
enum DronteMessageContents {
    Discrete(Note),
    Continuous(Option<Continous>),
}

#[derive(Debug)]
struct DronteEvent {
    contents: DronteMessageContents,
    timestamp: i64,
}

impl From<&[u8]> for DronteEvent {
    fn from(value: &[u8]) -> Self {
        let mut cont_number_buffer = [0u8; size_of::<u32>()];
        let mut cont_buffer = [0u8; size_of::<u32>()];
        let mut stamp_value = [0u8; size_of::<i64>()];

        value[15..19].iter().copied().enumerate().for_each(|(i,v)| {
            cont_number_buffer[i] = v;
        });
        for (i, v) in value[19..23].iter().copied().enumerate() {
            cont_buffer[i] = v;
        }

        let cont_number = u32::from_le_bytes(cont_number_buffer);
        let cont = u32::from_le_bytes(cont_buffer);

        let contents = if value[10] == 1 {
            if value[11] == 1 {
                DronteMessageContents::Discrete(Note::On(NoteValues {
                    channel: value[12],
                    value: value[13],
                    velocity: value[14],
                }))
            } else {
                DronteMessageContents::Discrete(Note::Off(NoteValues {
                    channel: value[12],
                    value: value[13],
                    velocity: value[14],
                }))
            }
        } else if cont_number != 0 {
            DronteMessageContents::Continuous(Some(Continous {
                number: cont_number,
                value: cont,
                channel: value[12],
            }))
        } else {
            DronteMessageContents::Continuous(None)
        };

        for (i, v) in value[23..31].iter().copied().enumerate() {
            stamp_value[i] = v;
        }
        let timestamp = i64::from_le_bytes(stamp_value);

        DronteEvent {
            contents,
            timestamp,
        }
    }
}

#[derive(Debug)]
struct DronteSynchro {
    ppq: f64,
}

impl From<&[u8]> for DronteSynchro {
    fn from(value: &[u8]) -> Self {
        let mut ppq_buffer = [0u8; 8];
        for (dex, v) in value[10..18].iter().enumerate() {
            ppq_buffer[dex] = *v;
        }

        Self {
            ppq: f64::from_le_bytes(ppq_buffer),
        }
    }
}


fn main() -> PdvResult<()> {
    let task1: Task<PdvResult<()>> = smol::spawn(async {
        let mut events_queue = subscribe("tcp://0.0.0.0:5555")?.bind()?;
        events_queue.set_subscribe("DRONTEEVNT")?;
        events_queue.set_subscribe("DRONTESYNC")?;

        while let Some(m) = events_queue.next().await {
            match m {
                Ok(m) => {
                    for message in m {
                        if message.starts_with("DRONTEEVNT".as_bytes()) {
                            println!("{:?}", DronteEvent::from(message.as_ref()));
                        } else if message.starts_with("DRONTESYNC".as_bytes()) {
                            println!("{:?}", DronteSynchro::from(message.as_ref()));
                        } else {
                            /*let mstr = message.as_str();
                            if let Some(str) = mstr {
                                println!("{}", str.to_string());
                            } else {
                                println!("{:?}", message);
                            }*/
                        }
                    }
                }
                Err(e) => eprintln!("{}", e.to_string()),
            }
        }

        Ok(())
    });

    smol::block_on(async {
        task1.await?;

        Ok(())
    })
}
