use crate::result::PdvError;
use async_zmq::{Error, SendError, SocketError, SubscribeError};

impl From<SocketError> for PdvError {
    fn from(value: SocketError) -> Self {
        PdvError(format!("SocketError => {}", value.to_string()))
    }
}

impl From<async_zmq::Error> for PdvError {
    fn from(value: Error) -> Self {
        PdvError(format!("ZmqError => {}", value.to_string()))
    }
}

impl From<async_zmq::SendError> for PdvError {
    fn from(value: SendError) -> Self {
        PdvError(format!("Zmq Send Error => {}", value.to_string()))
    }
}

impl From<async_zmq::SubscribeError> for PdvError {
    fn from(value: SubscribeError) -> Self {
        PdvError(format!("Zmq Subscribe Error => {}", value.to_string()))
    }
}
