mod froms;

use std::fmt::{Debug, Display, Formatter};

pub struct PdvError(pub String);

impl Display for PdvError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "PdvError: {}", self.0)
    }
}

impl Debug for PdvError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "PdvError: {}", self.0)
    }
}

impl std::error::Error for PdvError {}

pub type PdvResult<T> = Result<T, PdvError>;
